from django.urls import path
from . import views

app_name = 'users'

urlpatterns = [
    path('hello', views.say_hello, name='hello'),
    path('posts', views.get_post_list, name='post_list'),
    path('posts/create_from_form', views.create_post, name='create_post'),
    path('posts/<int:post_id>', views.get_post_detail, name='post_detail'),
]