from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from .models import Post
from .forms import PostForm

def say_hello(request):
    return HttpResponse("hello, user!")


def get_post_list(request):
    if request.method == 'POST':
        new_title = request.POST.get('new_title', '')
        content = request.POST.get('content', '')
        Post.objects.create(title=new_title, content=content)
    posts = Post.objects.all()
    context = {
        'post_list': posts
    }
    return render(request, 'users/get_posts.html', context)


def get_post_detail(request, post_id):
    print('-' * 10)
    print(request.GET)
    print('-' * 10)
    post = get_object_or_404(Post, id=post_id)
    # post = Post.objects.get(id=post_id)
    context = {
        'post': post
    }
    return render(request, 'users/post_detail.html', context)


def create_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
    else:
        form = PostForm()
    context = {
        'myform': form
    }
    return render(request, 'users/form_template.html', context)