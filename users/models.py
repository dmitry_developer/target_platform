from django.db import models
from django.contrib.auth.models import User


def upload_file(instance, filename):
    return '/'.join(['content', instance.title, filename])


class Post(models.Model):
    title = models.CharField(max_length=128)
    content = models.TextField()
    source = models.FileField(upload_to=upload_file, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f"{self.id}. {self.title}"